# CHANGELOG

<!--- next entry here -->

## 0.1.0
2020-06-15

### Features

- docker security (a53c2967098f807df84794cf8fe583346f2f9b40)

### Fixes

- add license (a1d7a2cdfbe4e8d185267206028642e1230f8b46)